import { Component } from '@angular/core';

import '@nx-example/shared/header-move';

@Component({
  selector: 'nx-example-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {}
